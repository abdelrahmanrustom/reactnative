import {
  GET_BRANCHES,
  GET_BRANCHES_SUCCESS,
  GET_BRANCHES_FAIL,
  DELETE_BRANCH,
  DELETE_BRANCH_SUCCESS,
  DELETE_BRANCH_FAIL,
  FILTER_BRANCHES
} from '../actions/types';

const INITIAL_STATE = {
  branches: [],
  keyword: '',
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_BRANCHES:
      return { ...state, loading: true, error: '' };
    case GET_BRANCHES_SUCCESS:
      return { ...state, branches: action.payload, loading: false };
    case GET_BRANCHES_FAIL:
      return { ...state, error: action.payload, loading: false };
    case DELETE_BRANCH:
      return { ...state, loading: true, error: '' };
    case DELETE_BRANCH_SUCCESS:
      return { ...state, loading: false };
    case DELETE_BRANCH_FAIL:
      return { ...state, error: action.payload, loading: false };
    case FILTER_BRANCHES:
      return { ...state, keyword: action.payload };
    default:
      return state;
  }
};
