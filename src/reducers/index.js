import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import BusinessReducer from './BusinessReducer';
import BusinessFormReducer from './BusinessFormReducer';
import NotificationReducer from './NotificationReducer';
import BranchReducer from './BranchReducer';

export default combineReducers({
  auth: AuthReducer,
  business: BusinessReducer,
  businessForm: BusinessFormReducer,
  notification: NotificationReducer,
  branch: BranchReducer
});
