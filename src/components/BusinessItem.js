import React from 'react';
import { Linking, TouchableOpacity, Text, View, Image, Alert } from 'react-native';

import { Actions } from 'react-native-router-flux';
import ModalDropdown from 'react-native-modal-dropdown';
import ActionSheet from 'react-native-actionsheet';
import { connect } from 'react-redux';
import { deleteBusiness } from '../actions';
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 7;
const options = ['Cancel', 'View branches', 'Edit', 'Favorites', 'Media', 'Delete'];
const title = '';
const state= '';
const BusinessItem = ({ business, onSubmit, deleteBusiness }) => {
  const { containerStyle, businessStyle, arrowStyle,iconStyle } = styles;
  const business_id = business.id;
  // var branch_id ;
  // if ( business.branch !== null) {
  //   branch_id = business.branch.id;
  // }
  return (
    <TouchableOpacity style={containerStyle} onPress={() => {
      console.log(business);
      state = business;

      //Actions.businessForm({ business, onSubmit });
      //Actions.branch_dashboard({business});
      //Actions.businessForm({ business, onSubmit });
      //deleteBusiness(business.id);
      // const business_id = business.id;
       //Actions.mediaList({ business_id });
       //this.popupDialog.show();
      this.Sheet.show();
      }}>


   

       <ActionSheet
        ref={o => this.Sheet = o}
        title={title}
        options={options}
        cancelButtonIndex={CANCEL_INDEX}
        destructiveButtonIndex={DESTRUCTIVE_INDEX}
        onPress={index => {
           // if (business.id !== null || business.branch.id !== null) {
           //  console.log(business.id);
           //  console.log(business.branch.id);
           //  }
           //console.log('cool  '+ business);
          const business = state;
          const business_id = state.id;
          //console.log(state);
         
          if (index == 1) Actions.branch_dashboard({business});
          if (index == 2) Actions.businessForm({ business, onSubmit });
          if (index == 3) Actions.userList({branch_or_bussiness:'business_id', business_id: business_id, list_type: 'saved-businesses' });
          if (index == 4) Actions.mediaList({ business_id });
          if (index == 5) {
            Alert.alert(
            '',
            'Are you sure you want to delete this business?',
            [
              { text: 'No' },
              {
                text: 'Yes',
                onPress: async () => {
                  deleteBusiness(business_id);
                }
              }
            ],
            // { cancelable: false }
          );
        }
        }}
      />

      <Text style={businessStyle}>{business.name}</Text>
      <View style={arrowStyle}>
        <Image style={iconStyle} source={require('../assets/right_arrow.png')} />
      </View>
    </TouchableOpacity>
  );
};
//eein@1234

const styles = {
  containerStyle: {
    borderRadius: 15,
    shadowColor: '#7c7c7c',
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.4,
    elevation: 2,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
    marginBottom: 5,
    padding: 15,
    backgroundColor: '#f9f9f9',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  businessStyle: {
    fontSize: 16,
    fontWeight: 'bold',
    backgroundColor: 'transparent'
  },
  arrowStyle: {
    alignSelf: 'center',
    position: 'absolute',
    right: 15
  },
  iconStyle: {
    width: 5,
    height: 10
  }
};

export default connect(null, { deleteBusiness })(BusinessItem);