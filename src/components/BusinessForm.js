import React, { Component } from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  ScrollView,
  View,
  Image,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
  BackHandler,
  Picker
} from 'react-native';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { connect } from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import {
  getCountries,
  getCities,
  getCategories,
  getSubcategories,
  getFlags,
  getInterests,
  submitBusiness
} from '../actions';
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Modal from 'react-native-modalbox';
import NavigationBar from './NavigationBar';
import SelectMultiple from 'react-native-select-multiple';
import MapView from 'react-native-maps';
import _ from 'lodash';

const { OS } = Platform;

class BusinessForm extends Component {
  state = {
    media: '',
    main_image: '',
    name_en: '',
    name_ar: '',
    phone: '',
    price: '0',
    price_name: 'Select price',
    description: '',
    descriptionAr: '',
    category: '',
    subcategory: '',
    category_name: 'Choose category',
    subcategory_name: 'Choose subcategory',
    website: '',
    fb_page: '',
    interests: [],
    interestsModalVisible: false
  };

  componentWillMount() {
    const {
      getCategories,
      getInterests,
      business
    } = this.props;
    getCategories();
    getInterests();
    if (business) {
      _.each(business, (value, key) => this.onPropChange(key, value));
      this.setState({
        category: business.category.id,
        subcategory: business.subcategory.id

      });
     // console.log(business.interests);
      this.setState({
          interests: business.interests.map(interest => {
              return { label: interest, value: interest };
          })
        });
      // TODO check here
      //console.log(business.branch.operation_hours);
      //const tokens = business.branch.operation_hours.split(/ |:/);
    }
  }

  componentWillReceiveProps(newProps) {
    const {
      getSubcategories,
      categories,
      subcategories,
      interests,
      business
    } = newProps;
 if (business) {
      
      if (!this.props.categories.length && categories.length) {
      
        this.setState({ category: business.category.id });
        getSubcategories(business.category.id);
      }
      if (!this.props.subcategories.length && subcategories.length) {
        this.setState({ subcategory: business.subcategory.id });
        
      }
      console.log('price: ' + business.price);
      const price = business.price + '';
      this.setState({ price: price });
    }
  }

  onPhotoPress() {
    ImagePicker.showImagePicker({ title: 'Add a photo', quality: 0.7 }, response => {
      if (!response.didCancel) {
        this.onPropChange('media', response);
      }
    });
  }

  renderPhoto() {
    const { media, main_image } = this.state;
    const { addPhotoStyle, photoStyle, textStyle } = styles;
    if (media) {
      return (
        <TouchableOpacity
          onPress={this.onPhotoPress.bind(this)}
          style={[addPhotoStyle, { backgroundColor: 'white' }]}
        >
          <Image style={photoStyle} source={media} />
        </TouchableOpacity>
      );
    }
    if (main_image) {
      return (
        <TouchableOpacity
          onPress={this.onPhotoPress.bind(this)}
          style={[addPhotoStyle, { backgroundColor: 'white' }]}
        >
          <Image style={photoStyle} source={{ uri: main_image }} />
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity
        onPress={this.onPhotoPress.bind(this)}
        style={addPhotoStyle}
      >
        <Image source={require('../assets/add_photo.png')} />
        <Text style={textStyle}>Add a photo</Text>
      </TouchableOpacity>
    );
  }

  onButtonPress() {
    const { business, submitBusiness, onSubmit } = this.props;
    const business_id = business && business.id; 
    const {
      media,
      name_en,
      name_ar,
      phone,
      website,
      fb_page,
      description,
      descriptionAr,
      category,
      subcategory,
      price,
      interests
      } = this.state;
    const interestsStr = interests.map(interest => interest.label).join(',');
    // console.log(
    //   media,
    //   name,
    //   nameAr,
    //   phone,
    //   website,
    //   facebook,
    //   description,
    //   descriptionAr,
    //   " category: "+category,
    //   " Subcategory: "+subcategory,
    //   " Price: "+price,
    //   interestsStr,
    //   business_id,
    //   );
      //if (subcategory === '') {
      //console.log(subcategory === ''? 'empttyy': subcategory);
      //}
      
    submitBusiness(
      media,
      name_en,
      name_ar,
      phone,
      website,
      fb_page,
      description,
      descriptionAr,
      subcategory !== ''? subcategory : category,
      price,
      interestsStr,
      business_id,
      onSubmit
    );
  }


  renderButton() {
    const { business, loading } = this.props;
    if (this.props.loading) return <Spinner style = {{marginBottom: 40}} />;
    return <Button color = '#623DD1' style = {{marginBottom: 40}} onPress={this.onButtonPress.bind(this)}>{business ? 'UPDATE' : 'CREATE'}</Button>;
  }

  onPropChange(key, value) {
    this.setState({ [key]: value });
  }

 


  renderCategoriesActionSheet() {
    let newCategoriesTitle = [];
    let newCategoriesValues= [];
    this.props.categories.map(category => {
      newCategoriesTitle.push(category.name);
      newCategoriesValues.push(category.id);
    })

    if(newCategoriesTitle.length !== 0) {
      return <TouchableOpacity style = {styles.actionsheetStyle} onPress={() => {
        this.Sheet.show();
        }}>
                <ActionSheet
          ref={o => this.Sheet = o}
          title={"Choose Category"}
          options={newCategoriesTitle}
          onPress={index => {
            this.onCategoryChange(newCategoriesValues[index], newCategoriesTitle[index])
          }}
        />
          <Text style = {styles.textStyleBlack}> {this.state.category_name}</Text> 
        </TouchableOpacity>;
    }
  }


  renderSubCategoriesActionSheet() {
    let newSubCategoriesTitle = [];
    let newSubCategoriesValues= [];
    this.props.subcategories.map(subcategory => {
      newSubCategoriesTitle.push(subcategory.name);
      newSubCategoriesValues.push(subcategory.id);
    })

    if(newSubCategoriesTitle.length !== 0) {
      return <TouchableOpacity style = {styles.actionsheetStyle} onPress={() => {
        this.SubSheet.show();
        }}>
                <ActionSheet
          ref={o => this.SubSheet = o}
          title={"Choose Subcategory"}
          options={newSubCategoriesTitle}
          onPress={index => {
            this.onPropChange('subcategory', newSubCategoriesValues[index]);
            this.onPropChange('subcategory_name', newSubCategoriesTitle[index]);
          }}
        />
          <Text style = {styles.textStyleBlack}> {this.state.subcategory_name}</Text> 
        </TouchableOpacity>;
    }
  }

  renderPriceActionSheet(){
    let priceValues = ['1', '2', '3', '4', '5'];
    let priceTitle= ['$', '$$', '$$$', '$$$$', '$$$$$'];
 

      return <TouchableOpacity style = {styles.actionsheetStyle} onPress={() => {
        this.priceSheet.show();
        }}>
                <ActionSheet
          ref={o => this.priceSheet = o}
          title={"Choose Price"}
          options={priceTitle}
          onPress={index => {
            this.onPropChange('price', priceValues[index])
            this.onPropChange('price_name', priceTitle[index])
            
          }}
        />
          <Text style = {styles.textStyleBlack}> {this.state.price_name}</Text> 
        </TouchableOpacity>;
  }

  onCategoryChange(value, name) {
    this.onPropChange('category', value);
    this.onPropChange('category_name', name);
    this.onPropChange('subcategory', '');
    this.props.getSubcategories(value);
  }

 
  render() {
    const {
      scrollViewStyle,
      imageStyle,
      containerStyle,
      addPhotoStyle,
      photoStyle,
      textContainerStyle,
      textStyle,
      selectStyle,
      timeStyle,
      signUpStyle
    } = styles;
    const {
      category,
      subcategory,
      price,
      interests,
      interestsModalVisible
    } = this.state;
    const { interestsModal } = this.refs;
    const fields = [
      { name: 'name_en', placeholder: 'Name' },
      { name: 'name_ar', placeholder: 'Arabic name' },
      { name: 'phone', placeholder: 'Phone number' },
      { name: 'website', placeholder: 'Website' },
      { name: 'fb_page', placeholder: 'Facebook' },
      { name: 'description', placeholder: 'Description', multiline: true },
      { name: 'descriptionAr', placeholder: 'Arabic description', multiline: true }
    ];
 
    return (
      //<KeyboardAvoidingView behavior={OS === 'ios' ? "padding" : null}>
        <View style={{flex: 1}}>
        <ScrollView style={scrollViewStyle}>
          <Card style={imageStyle}>
            <CardSection style={containerStyle}>
              {this.renderPhoto()}
              {fields.map(field => {
                if (field.name ==='phone') {
                    return (
                      <Input
                      key={field.name}
                      style = {{marginTop:11}}
                      value={this.state[field.name]}
                      onChangeText={this.onPropChange.bind(this, field.name)}
                      placeholder={field.placeholder}
                      type="dark"
                      textAlign="left"
                      multiline={field.multiline}
                      keyboardType = "phone-pad"
                      />
                    );   
                }
                return (
                  <Input
                    key={field.name}
                    value={this.state[field.name]}
                    onChangeText={this.onPropChange.bind(this, field.name)}
                    placeholder={field.placeholder}
                    type="dark"
                    textAlign="left"
                    style = {{marginTop:11}}
                    multiline={field.multiline}
                  />
                );
              })}
      <View style = {{flexDirection : 'row'}}>    
        {this.renderCategoriesActionSheet()}
        
      </View>
      <View style = {{flexDirection : 'row'}}> 
        {this.renderSubCategoriesActionSheet()}
      </View>
      <View style = {{flexDirection : 'row'}}> 
        {this.renderPriceActionSheet()}
      </View>

             
              
              <TouchableOpacity
                style={timeStyle}
                onPress={() => interestsModal.open()}
              >
                <Text>Choose interests ({interests.length} selected)</Text>
              </TouchableOpacity>
              {this.renderButton()}
            </CardSection>
          </Card>
        </ScrollView>

        <Modal
          ref="interestsModal"
          onRequestClose={() => null}
          style={{ height: '50%' }}
          backdrop
        >
          <SelectMultiple
            items={this.props.interests.map(interest => {
              return { label: interest.name || interest.label, value: interest.name || interest.label };
            })}
            selectedItems={interests.map(interest => {
              return { label: interest.name || interest.label, value: interest.name || interest.label };
            })}
            onSelectionsChange={interests => this.onPropChange('interests', interests.map(interest => {
              return { label: interest.name || interest.label, value: interest.name || interest.label };
            }))}
          />
        </Modal>
         <View style={{position: 'absolute', left: 0, right: 0, bottom: 0}}>
        <NavigationBar currentScene = {'add'} />
        </View>
        </View>
      //</KeyboardAvoidingView>
    );
  }
}

const styles = {
  scrollViewStyle: {
    marginBottom: 5
  },
  imageStyle: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  containerStyle: {
    display: 'flex',
  	flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  addPhotoStyle: {
    width: '100%',
    height: 120,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#242424',
    marginBottom: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoStyle: {
  	width: '100%',
    height: 120,
    resizeMode: 'contain',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  textContainerStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  textStyle: {
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 10
  },
  textStyleBlack: {
    color: 'black',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 10
  },
  selectStyle: {
    flex: 1,
    width: '100%',
    height: OS === 'android' ? 40 : undefined,
    borderRadius: 10,
    backgroundColor: '#f9f9f9',
    marginBottom: 10,
    shadowColor: '#7c7c7c',
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.4,
    elevation: 2
  },
  timeStyle: {
    padding: 5,
    borderRadius: 10,
    backgroundColor: '#f9f9f9',
    marginBottom: 10
  },
  signUpStyle: {
    textDecorationLine: 'underline'
  },
  actionsheetStyle :{
    borderWidth: 1,
    borderRadius: 2,
    height: 40,
    marginBottom: 10,
    borderColor: '#DDD',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    marginTop: 10,
    flex: 1
  }

};

const mapStateToProps = ({ businessForm }) => {
  const {
    countries,
    cities,
    categories,
    subcategories,
    flags,
    interests,
    error,
    loading
  } = businessForm;
  return {
    countries,
    cities,
    categories,
    subcategories,
    flags,
    interests,
    error,
    loading
  };
};

export default connect(mapStateToProps, {
  getCountries,
  getCities,
  getCategories,
  getSubcategories,
  getFlags,
  getInterests,
  submitBusiness
})(BusinessForm);
