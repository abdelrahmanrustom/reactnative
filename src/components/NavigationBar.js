import React, { Component } from 'react';
import { View, Image, TouchableWithoutFeedback } from 'react-native';
import { Actions } from 'react-native-router-flux';

class NavigationBar extends Component { 
render(){
   // console.log(this.props.business);
   // console.log(this.props.currentScene);
  return (
    <View style={{ width: '100%' }}>
      <View
        style={{
          height: 60,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          backgroundColor: 'white',
          overflow: 'hidden'
        }}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            if (Actions.currentScene !== 'dashboard') {
              Actions.dashboard({ type: 'replace' });
            }
          }}
        >
          {this.renderHomeIcon()}
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => { 
              //console.log(this.props.business); 
              const onSubmit = this.props.onSubmit;
              const business = this.props.business;
              if (this.props.business) {
                if (Actions.currentScene !== 'branchForm') {
                  Actions.branchForm({business, onSubmit});
                }
              } else {
                  if (Actions.currentScene !== 'businessForm') {
                    Actions.businessForm({ onSubmit });
                  }
                }   
          }}
        >
          {this.renderAddIcon()}
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            if (Actions.currentScene !== 'settings') {
              Actions.settings({ type: 'replace' });
            }
          }}
        >
          {this.renderSettingsIcon()}
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};

renderHomeIcon() {
  if (this.props.currentScene === 'home') {
    //return (<Image source={require('../assets/home_icon_focused.png')} />); 
    return (<Image style={{ width: 25, height: 25}} height = {25} width= {25} source={{uri: 'home_icon_focused'}} />);
  } else {
    //return (<Image source={require('../assets/home_icon.png')} />);
    return (<Image style={{ width: 25, height: 25}} height = {25} width= {25} source={{uri: 'home_icon'}} />);
  }
}

renderAddIcon() {
  if (this.props.currentScene === 'add') {
    return (<Image style={{ position: 'relative', bottom: 3, height: 48,  width: 48 }} height = {48} width= {42} source={{uri: 'add_icon_focused'}} />);
  } else {
    return (<Image style={{ position: 'relative', bottom: 3, height: 48,  width: 48  }} height = {48} width= {42} source={{uri: 'add_icon'}} />);
  }
}

renderSettingsIcon() {
  if (this.props.currentScene === 'settings') {
    return (<Image style={{ width: 25, height: 25}} height = {25} width= {25} source={{uri: 'settings_icon_focused'}} />);
   // return (<Image style={{ width: 25, height: 25 }} source={require('../assets/settings_icon_focused.png')} />); 
  } else {
    return (<Image style={{ width: 25, height: 25}} height = {25} width= {25} source={{uri: 'settings_icon'}} />);
    //return (<Image style={{ width: 25, height: 25 }} source={require('../assets/settings_icon.png')} />);
  }
}

}
export default NavigationBar;