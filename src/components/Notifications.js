import React, { Component } from 'react';
import { View, ListView, RefreshControl, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { getNotifications } from '../actions';
import NotificationItem from './NotificationItem';
import CommentNotification from './CommentNotification';
import NavigationBar from './NavigationBar';

class Notifications extends Component {
   state = {
    page: 1,
    notifications: [],
    notificationsAdded: false
  };

constructor() {
    super()
    this.onLoadMore = this.onLoadMore.bind(this)
  }
  componentWillMount() {
    this.props.getNotifications(this.state.page);
    //this.createDataSource(this.props);
    //this.dataSource = this.props.notifications
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.notifications.length !== 0 && this.state.notificationsAdded === false) {
      console.log(nextProps.notifications);
      var newNotifications = this.state.notifications;
      newNotifications.push(...nextProps.notifications.notifications)
      console.log(newNotifications);
      this.setState({notifications: newNotifications, notificationsAdded: true});
    }
  }


  onRefresh() {
    this.props.getNotifications(this.state.page);
  }

  onLoadMore() {
    console.log("onLoadMore "+ this.state.page);
    const { page, reachedLastItem, totalPage, show } = this.state
    if (page !== this.props.notifications.pagination.total_pages_no) {
      const nextPage = page + 1;
      this.setState({page: nextPage, notificationsAdded: false});
      this.props.getNotifications(nextPage);
    }
  }

  render() {
    const { containerStyle, listViewStyle } = styles;
    //console.log("render() "+this.props.notifications);
    return (
      <View style={containerStyle}>
        <FlatList
          style={listViewStyle}
          data={this.state.notifications}
          renderItem={({ item }) => {
          console.log("renderRow " + item.data.type);
          if (item.data.type === 'comment') return <CommentNotification notification={item} />;
          return <NotificationItem notification={item} />;
          }}
          onEndReached={this.onLoadMore}
          onEndReachedThreshold={0.7}
          keyExtractor={(notification, index) => index}
          refreshControl={
            <RefreshControl
              refreshing={this.props.loading}
              onRefresh={this.onRefresh.bind(this)}
            />
          }
        />
        <NavigationBar />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  listViewStyle: {
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    marginLeft: 5,
    marginRight: 5,
    paddingTop: 5,
    marginBottom: 5,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15
  }
};

const mapStateToProps = ({ notification }) => {
  const { notifications, error, loading } = notification;
  return { notifications, error, loading };
};

export default connect(mapStateToProps, { getNotifications })(Notifications);
