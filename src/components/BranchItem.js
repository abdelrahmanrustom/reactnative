import React from 'react';
import { Linking, TouchableOpacity, Text, View, Image, Alert } from 'react-native';
import { Card, CardSection } from './common';
import { Actions } from 'react-native-router-flux';
import ModalDropdown from 'react-native-modal-dropdown';
import ActionSheet from 'react-native-actionsheet';
import { connect } from 'react-redux';
import { deleteBranch } from '../actions';
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 7;
const options = ['Cancel', 'View', 'Edit', 'Checkins', 'Reviews', 'Media', 'Delete'];
const title = '';
const state= '';
const BranchItem = ({ branch, onSubmit, deleteBranch }) => {
  //console.log(branch);
  const { containerStyle, businessStyle, arrowStyle,iconStyle } = styles;
  const branch_id = branch.id;
  return (
    <TouchableOpacity style={containerStyle} onPress={() => {
      //console.log(branch);
      state = branch;
      // const businessURL = 'http://myblabber.com/business-detail.php?id=' + branch.business_id+'&branch_id='+branch_id;
      // Linking.openURL(businessURL);
      // Actions.branchForm({ branch, onSubmit });
      // deleteBranch(branch_id, branch.business_id); //done
      // Actions.reviewList({ branch_id });  //done
      // Actions.userList({business_id: branch_id, list_type: 'checkins', branch_or_bussiness: 'branch_id' }); //done
      // Actions.userList({branch_or_bussiness:'business_id', business_id: branch.business_id, list_type: 'saved-businesses' }); //done for business
      // Actions.mediaList({ branch_id });
     this.ActionSheet.show()
      }}>
      <ActionSheet
        ref={o => this.ActionSheet = o}
        title={title}
        options={options}
        cancelButtonIndex={CANCEL_INDEX}
        destructiveButtonIndex={DESTRUCTIVE_INDEX}
        onPress={index => {

          console.log(state);
          const branch = state;
          const branch_id = branch.id;
          const businessURL = 'http://myblabber.com/business-detail.php?id=' + state.business_id + '&branch_id=' + state.id;
          
          if (index == 1) Linking.openURL(businessURL); //done
          if (index == 2) Actions.branchForm({ branch, onSubmit });
          if (index == 3)  Actions.userList({business_id: branch_id, list_type: 'checkins', branch_or_bussiness: 'branch_id' }); //done
          if (index == 4) Actions.reviewList({ branch_id });
          if (index == 5) Actions.mediaList({ branch_id });
          if (index == 6) {
            Alert.alert(
            '',
            'Are you sure you want to delete this business?',
            [
              { text: 'No' },
              {
                text: 'Yes',
                onPress: async () => {
                  deleteBranch(branch_id, branch.business_id);
                }
              }
            ],
            // { cancelable: false }
            );
          }
      
        }}
      />
      <Text style={businessStyle}>{branch.name}</Text>
      <View style={arrowStyle}>
        <Image style={iconStyle} source={require('../assets/right_arrow.png')} />
      </View>
    </TouchableOpacity>
  );
};

 
const styles = {
  containerStyle: {
    borderRadius: 15,
    shadowColor: '#7c7c7c',
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.4,
    elevation: 2,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
    marginBottom: 5,
    padding: 15,
    backgroundColor: '#f9f9f9',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  businessStyle: {
    fontSize: 16,
    fontWeight: 'bold',
    backgroundColor: 'transparent'
  },
  arrowStyle: {
    alignSelf: 'center',
    position: 'absolute',
    right: 15
  },
  iconStyle: {
    width: 5,
    height: 10
  }
};

export default connect(null, { deleteBranch })(BranchItem);