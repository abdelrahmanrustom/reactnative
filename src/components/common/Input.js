import React from 'react';
import { View, Text, TextInput } from 'react-native';

const Input = ({
    style,
    label,
    value,
    type,
    textAlign,
    onChangeText,
    placeholder,
    secureTextEntry,
    multiline,
    borderRadius,
    keyboardType
  }) => {
  const { containerStyle, labelStyle, inputStyle } = styles;
  const color = type === 'dark' ? 'black' : 'white';
  const placeholderColor = type === 'dark' ? '#8d8d8d' : 'white';
  const kType = keyboardType ? keyboardType: 'default';
  return (
    <View style={[style, containerStyle]}>
      <TextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        underlineColorAndroid='transparent' 
        placeholderTextColor={placeholderColor}
        style={[inputStyle, { color, textAlign}, borderRadius]}
        autoCorrect={false}
        value={value}
        onChangeText={onChangeText}
        multiline={multiline}
        keyboardType={kType}
      />
    </View>
  );
};

const styles = {
  containerStyle: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputStyle: {
    height: 40,
    paddingRight: 5,
    paddingLeft: 5,
    marginBottom: 30,
    fontSize: 12,
    lineHeight: 12,
    borderColor: '#C0CFDE',
    borderWidth: 1,
    flex: 1,
    textAlign: 'center'
  }
};

export { Input };
