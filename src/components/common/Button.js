import React from 'react';
import { TouchableWithoutFeedback, TouchableOpacity, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Button = ({ disabled, color, style, onPress, children }) => {
  const { buttonStyle, textStyle } = styles;
  const Touchable = disabled ? TouchableWithoutFeedback : TouchableOpacity;
  return (
    <Touchable onPress={disabled ? () => {} : onPress} style={[buttonStyle, style]}>
      <LinearGradient colors={disabled ? ['#ddd', '#aaa'] : [color, color]} style={buttonStyle}>
        <Text style={textStyle}>{children}</Text>
      </LinearGradient>
    </Touchable>
  );
};

const styles = {
  buttonStyle: {
    alignSelf: 'stretch',
    height: 40,
    borderRadius: 25,
  },
  textStyle: {
    alignSelf: 'center',
    color: '#FFF',
    backgroundColor: 'transparent',
    fontSize: 12,
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 10
  }
};

export { Button };
