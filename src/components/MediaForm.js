import React, { Component } from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  Picker
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { submitMedia } from '../actions';

const { OS } = Platform;

class MediaForm extends Component {
  state = {
    media: '',
    type: '',
    caption: '',
    rating: ''
  };

   componentWillMount() {
    const { business_id } = this.props;
    business_id? this.setState({type: 'menu'}): this.setState({type: 'image'}) 
  }
  onPhotoPress() {
    const {type} = this.state;
    if (type === "brochure") {
        DocumentPicker.show({
        filetype: [DocumentPickerUtil.pdf()],
      },(error,res) => {
        // // Android
        console.log(
           res
        );
        this.onPropChange('media', res);
        //alert(res.type);
      });
    } else {
      ImagePicker.showImagePicker({ title: 'Add a photo' }, response => {
        if (!response.didCancel) {
          this.onPropChange('media', response);
          console.log(response);
        }
      });
    }
  }

  renderPhoto() {
    const { media } = this.state;
    const { addPhotoStyle, photoStyle, textStyle } = styles;
    if (media) {
      return (
        <TouchableOpacity
          onPress={this.onPhotoPress.bind(this)}
          style={[addPhotoStyle, { backgroundColor: 'white' }]}
        >
          <Image style={photoStyle} source={media} />
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity
        onPress={this.onPhotoPress.bind(this)}
        style={addPhotoStyle}
      >
        <Image source={require('../assets/add_photo.png')} />
        <Text style={textStyle}>Add a photo</Text>
      </TouchableOpacity>
    );
  }

  onButtonPress() {
    const { business_id, submitMedia, onSubmit, branch_id } = this.props;
    const { media, type, caption, rating } = this.state;
    submitMedia(business_id, branch_id, media, type, caption, rating, onSubmit);
  }

  renderButton() {
    const { loading } = this.props;
    if (this.props.loading) return <Spinner />;
    return <Button color = '#623DD1' onPress={this.onButtonPress.bind(this)}>CREATE</Button>;
  }

  renderPicker() {
    const { business_id } = this.props;
     //console.log('busineesid: '+ business_id + ' branch_id: '+ branch_id);
  //  var item = business_id?[ 'menu', 'product', 'brochure']: ['image'];
    var item = business_id?[ 'menu', 'product', 'brochure']: ['image'];

    return <Picker
                style={styles.selectStyle}
                selectedValue={this.state.type}
                onValueChange={(value, index) => this.onPropChange('type', value)}
              >
                {item.map(i => <Picker.Item key={i} label={i} value={i} />)}
              </Picker>
  }

  onPropChange(key, value) {
    this.setState({ [key]: value });
  }
  
  render() {
    const {
      scrollViewStyle,
      imageStyle,
      containerStyle,
      addPhotoStyle,
      photoStyle,
      textContainerStyle,
      textStyle,
      selectStyle,
      timeStyle,
      signUpStyle
    } = styles;
    const { media, type, caption, rating } = this.state;
    const fields = [
      { name: 'caption', placeholder: 'Caption', multiline: true }
    ];
    return (
      <KeyboardAvoidingView behavior={OS === 'ios' ? "padding" : null}>
        <ScrollView style={scrollViewStyle}>
          <Card style={imageStyle}>
            
              {this.renderPhoto()}
              {fields.map(field => {
                return (
                  <Input
                    key={field.name}
                    value={this.state[field.name]}
                    onChangeText={this.onPropChange.bind(this, field.name)}
                    placeholder={field.placeholder}
                    type="dark"
                    textAlign="left"
                    multiline={field.multiline}
                  />
                );
              })}
              <Picker
                style={selectStyle}
                selectedValue={rating}
                onValueChange={(value, index) => this.onPropChange('rating', value)}
              >
                <Picker.Item label="Choose rating" value="0" />
                {['1', '2', '3', '4', '5'].map(i => <Picker.Item key={i} label={i} value={i} />)}
              </Picker>
              {this.renderPicker()}
              {this.renderButton()}
            
          </Card>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = {
  scrollViewStyle: {
    marginBottom: 5
  },
  imageStyle: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  containerStyle: {
    display: 'flex',
  	flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  addPhotoStyle: {
    width: '100%',
    height: 400,
    backgroundColor: '#242424',
    marginBottom: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoStyle: {
  	width: '100%',
    height: 400,
    resizeMode: 'contain'
  },
  textContainerStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  textStyle: {
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 10
  },
  selectStyle: {
    flex: 1,
    width: '100%',
    height: OS === 'android' ? 40 : undefined,
    borderRadius: 10,
    backgroundColor: '#f9f9f9',
    marginBottom: 10,
    shadowColor: '#7c7c7c',
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.4,
    elevation: 2
  },
  timeStyle: {
    padding: 5,
    borderRadius: 10,
    backgroundColor: '#f9f9f9',
    marginBottom: 10
  },
  signUpStyle: {
    textDecorationLine: 'underline'
  }
};

const mapStateToProps = ({ businessForm }) => {
  const { error, loading } = businessForm;
  return { error, loading };
};

export default connect(mapStateToProps, { submitMedia })(MediaForm);
