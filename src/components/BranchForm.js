import React, { Component } from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  ScrollView,
  View,
  Image,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
  BackHandler,
  Picker
} from 'react-native';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { connect } from 'react-redux';
import {
  getFlags,
  getAreas,
  submitBranch
} from '../actions';
import NavigationBar from './NavigationBar';
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Modal from 'react-native-modalbox';
import SelectMultiple from 'react-native-select-multiple';
import MapView from 'react-native-maps';
import _ from 'lodash';

const { OS } = Platform;

class BranchForm extends Component {
  state = {
    name: '',
    nameAr: '',
    address: '',
    addressAr: '',
    phone: '',
    lat: '',
    lng: '',
    operation_hours: [],
    area: '',
    areas: [],
    flags: [],
    Sundayfrom: false,
    Mondayfrom: false,
    Tuesdayfrom: false,
    Wednesdayfrom: false,
    Thursdayfrom: false,
    Fridayfrom: false,
    Saturdayfrom: false,
    Sundayto: false,
    Mondayto: false,
    Tuesdayto: false,
    Wednesdayto: false,
    Thursdayto: false,
    Fridayto: false,
    Saturdayto: false,
    region: {
      latitude: 30.042,
      longitude: 31.252,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    },
    flagsModalVisible: false
  };

  componentWillMount() {
   // console.log(this.props.branch);
    const {
      getFlags,
      branch,
      getAreas,
      business
    } = this.props;
   //getCountries();
    //console.log(business.name);
    getFlags();
    var op_hours = [];
    if (branch) {
      _.each(branch, (value, key) => this.onPropChange(key, value));
      this.setState({
        area: branch.area.id
      });
      getAreas(branch.city.id);
      branch.operation_hours.map(operation_hour => {
      const operation_hour_format = operation_hour.split(' :from');
      const day = operation_hour_format[0];
      const times = operation_hour_format[1].split(' to ');
      if (times[0].includes('am')) {
          currentTimeFrom = times[0].replace('am','');
          currentTimeFrom = times[0].replace('am','');
          fromHours = 0;
      } else {0
        currentTimeFrom = times[0].replace('pm','');
         fromHours = 12;
      }
      if (times[1].includes('am')) {
          currentTimeTo = times[1].replace('am','');
          toHours = 0;
      } else {
          toHours = 12;
          currentTimeTo = times[1].replace('pm','');
      }
      const fromTime = new Date(1970, 1, 1, fromHours+ parseInt(currentTimeFrom.split(':')[0]), parseInt(currentTimeFrom.split(':')[1]), 0, 0);
      const toTime = new Date(1970, 1, 1, toHours+parseInt(currentTimeTo), 0, 0, 0);
      op_hours.push({[day]:fromTime+'/'+toTime});
      //console.log(obj);
      //console.log(fromTime);
      });
      const tokens = branch.operation_hours;
      const fromHours = parseInt(tokens[1]) + (tokens[3] === 'PM' ? 12 : 0);
      if (fromHours === 12) fromHours = 0;
      if (fromHours === 24) fromHours = 12;
      const toHours = parseInt(tokens[5]) + (tokens[7] === 'PM' ? 12 : 0);
      if (toHours === 12) toHours = 0;
      if (toHours === 24) toHours = 12;
    
      this.setState({
        region: {
          latitude: parseFloat(branch.lat),
          longitude: parseFloat(branch.lng),
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        },
        operation_hours : op_hours
      })
      //console.log(op_hours);
    } else {
      if(business){
        getAreas('1');
        op_hours.push({'Sunday':'Sun Feb 01 1970 09:00:00 GMT-0800 (Pacific Standard Time)/Sun Feb 01 1970 17:00:00 GMT-0800 (Pacific Standard Time)'});
        op_hours.push({'Monday':'Sun Feb 01 1970 09:00:00 GMT-0800 (Pacific Standard Time)/Sun Feb 01 1970 17:00:00 GMT-0800 (Pacific Standard Time)'});
        op_hours.push({'Tuesday':'Sun Feb 01 1970 09:00:00 GMT-0800 (Pacific Standard Time)/Sun Feb 01 1970 17:00:00 GMT-0800 (Pacific Standard Time)'});
        op_hours.push({'Wednesday':'Sun Feb 01 1970 09:00:00 GMT-0800 (Pacific Standard Time)/Sun Feb 01 1970 17:00:00 GMT-0800 (Pacific Standard Time)'});
        op_hours.push({'Thursday':'Sun Feb 01 1970 09:00:00 GMT-0800 (Pacific Standard Time)/Sun Feb 01 1970 17:00:00 GMT-0800 (Pacific Standard Time)'});
        op_hours.push({'Friday':'Sun Feb 01 1970 09:00:00 GMT-0800 (Pacific Standard Time)/Sun Feb 01 1970 17:00:00 GMT-0800 (Pacific Standard Time)'});
        op_hours.push({'Saturday':'Sun Feb 01 1970 09:00:00 GMT-0800 (Pacific Standard Time)/Sun Feb 01 1970 17:00:00 GMT-0800 (Pacific Standard Time)'});
        this.setState({operation_hours : op_hours});
      }
    }

  }

  // componentWillReceiveProps(newProps) {
  //   const {
  //     getCities,
  //     countries,
  //     cities,
  //     branch
  //   } = newProps;
  //   if (branch) {
  //     if (!this.props.countries.length && countries.length) {
  //       this.setState({ country: branch.country.id });
  //       getCities(branch.country.id);
  //     }
  //     if (!this.props.cities.length && cities.length) {
  //       this.setState({ city: branch.city.id });
  //     }
  //   }
  // }

  onButtonPress() {
    const { countries, cities, branch, business, submitBranch, onSubmit } = this.props;

    const {
      name,
      nameAr,
      address,
      addressAr,
      area,
      phone,
      operation_hours,
      region,
      lat,
      lng,
      flags
    } = this.state;
    //console.log(nameAr);
    const flagsStr = flags.map(flag => flag.id || flag.value).join(',');
    //const interestsStr = interests.map(interest => interest.label).join(',');
    const { latitude, longitude } = region;
// Sunday: from 09:00 am to 05:00 pm
    var op_hours_to_be_submitted = '';
     operation_hours.map(operation_hour => { 
      var day = Object.keys(operation_hour)[0];
      const operation_hour_format = operation_hour[day].split('/');
      dateTo = new Date(operation_hour_format[1]);
      dateFrom = new Date(operation_hour_format[0]);
      var timeTo, timeFrom, hours, minutes;
      if (dateTo.getHours() > 12) {
        if (dateTo.getHours() == 12) {
          hours = dateTo.getHours();
        } else
          hours = dateTo.getHours() - 12;
        minutes = dateTo.getMinutes();
        if (minutes < 10) {
          minutes = '0'+minutes;
        }
        if (hours < 10) {
          hours = '0'+hours;
        }
        timeTo = 'to ' + hours+':'+minutes + ' pm';
      } else {
        hours = dateTo.getHours();
        minutes = dateTo.getMinutes();
        if (minutes < 10) {
          minutes = '0'+minutes;
        }
        if (hours < 10) {
          hours = '0'+hours;
        }
        timeTo = 'to ' + hours + ':' + minutes + ' am';
      }
      if (dateFrom.getHours() > 12) {
         if (dateFrom.getHours() == 12) {
          hours = dateFrom.getHours();
        } else
          hours = dateFrom.getHours() - 12;
        minutes = dateFrom.getMinutes();
        if (minutes < 10) {
          minutes = '0'+minutes;
        }
        if (hours < 10) {
          hours = '0'+hours;
        }
        timeFrom = 'from '+ hours + ':' + minutes + ' pm ';
      } else {
        hours = dateFrom.getHours();
        minutes = dateFrom.getMinutes();
        if (minutes < 10) {
          minutes = '0'+minutes;
        }
        if (hours < 10) {
          hours = '0'+hours;
        }
        timeFrom = 'from ' + hours + ':' + minutes + ' am ';
      }
      day = day + ': '+ timeFrom+ timeTo;
      if (op_hours_to_be_submitted === '') {
        op_hours_to_be_submitted = day;
      } else {
        op_hours_to_be_submitted = op_hours_to_be_submitted + ', ' + day;
      }
    });

    //console.log("area: "+ area + " area.name: "+ area.name+ " area.id: "+ area.id);
     // console.log(' address: ' + address+ ' addressAr: '+
     // addressAr+ ' phone: '+ phone+ ' Operation: '+ op_hours_to_be_submitted+ ' lat: '+ lat+' flags: '+ flags);
     // console.log('lat: '+ lat + ' long: '+ lng);
    submitBranch(
      address,
      addressAr,
      phone,
      op_hours_to_be_submitted,
      flagsStr,
      lat+'',
      lng+'',
      branch? branch.id: undefined,
      business? business.id: undefined,
      onSubmit
    );
  }

  renderButton() {
    const { branch, loading } = this.props;
    if (this.props.loading) return <Spinner style = {{marginBottom: 40}}/>;
    return <Button color = '#623DD1' style = {{marginBottom: 40}} onPress={this.onButtonPress.bind(this)}>{branch ? 'UPDATE' : 'CREATE'}</Button>;
  }

  onPropChange(key, value) {
    this.setState({ [key]: value });
  }

  // renderCountries() {
  //   return this.props.countries.map(country => {
  //     return <Picker.Item key={country.id} label={country.name} value={country.id} />;
  //   });
  // }
  // renderAreas() {
  //   return this.props.areas.map(area => {
  //     return <Picker.Item key={area.id} label={area.name} value={area.id} />;
  //   });
  // }
   renderOperationHours() {
    const { operation_hours } = this.state;
//    console.log(operation_hours);
    var dateTo, dateFrom, counter;
    counter = 0;
    return operation_hours.map(operation_hour => { 
      const day = Object.keys(operation_hour)[0];
      const operation_hour_format = operation_hour[day].split('/');
      dateTo = new Date(operation_hour_format[1]);
      dateFrom = new Date(operation_hour_format[0]);
      var dayFromState = day+'from';
      var dayToState = day + 'to';
      counter++;
      var timeFrom, timeTo, hoursTo, minutesTo, hoursFrom, minutesFrom;
      if (dateTo.getHours() >= 12) {
         if (dateTo.getHours() == 12) {
          hoursTo = dateTo.getHours();
        } else
          hoursTo = dateTo.getHours() - 12;
        minutesTo = dateTo.getMinutes();
        if (minutesTo < 10) {
          minutesTo = '0'+minutesTo;
        }
        timeTo = hoursTo+':'+minutesTo + ' pm';
      } else {
        hoursTo = dateTo.getHours();
        minutesTo = dateTo.getMinutes();
        if (minutesTo < 10) {
          minutesTo = '0'+minutesTo;
        }
        timeTo = hoursTo + ':' + minutesTo + ' am';
      }

      if (dateFrom.getHours() >= 12) {
         if (dateFrom.getHours() == 12) {
          hoursFrom = dateFrom.getHours();
        } else
          hoursFrom = dateFrom.getHours() - 12;
        minutesFrom = dateFrom.getMinutes();
        if (minutesFrom < 10) {
          minutesFrom = '0'+minutesFrom;
        }
        timeFrom = hoursFrom + ':' + minutesFrom + ' pm ';
      } else {
        hoursFrom = dateFrom.getHours();
        minutesFrom = dateFrom.getMinutes();
        if (minutesFrom < 10) {
          minutesFrom = '0'+minutesFrom;
        }
        timeFrom = hoursFrom + ':' + minutesFrom + ' am ';
      }
      //console.log('time from '+ timeFrom);
      //console.log('time to '+ timeTo);
      return <View style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10
              }}
              key={counter}>
                <Text style={{marginTop: 5}}>{day}</Text>
                <TouchableOpacity
                  style={styles.timeStyle}
                  onPress={() => { //console.log(this.state[dayFromState]);
                    this.setState({ [dayFromState]: true });
                   //console.log(this.state[dayFromState]);
                 }}>
                  <Text>{timeFrom}</Text>
                </TouchableOpacity>
                <Text style={{marginTop: 5}}> - </Text>
                <TouchableOpacity
                  style={styles.timeStyle}
                  onPress={() => this.setState({ [dayToState]: true })}
                >
                  <Text>{timeTo}</Text>
                </TouchableOpacity>
                <DateTimePicker
                  isVisible={this.state[dayFromState]}
                  onConfirm={time => this.handleFromPickerConfirm(time, day)}
                  onCancel={() => this.setState({ [dayFromState]: false })}
                  mode="time"
                  is24Hour={false}
                  date={dateFrom}
                />
                <DateTimePicker
                  isVisible={this.state[dayToState]}
                  is24Hour={false}
                  onConfirm={time => this.handleToPickerConfirm(time, day)}
                  onCancel={() => this.setState({ [dayToState]: false })}
                  mode="time"
                  date={dateTo}
                />
              </View>;
    });
  }

  // renderCities() {
  //   return this.props.cities.map(city => {
  //     return <Picker.Item key={city.id} label={city.name} value={city.id} />;
  //   });
  // }

  // onCountryChange(value) {
  //   this.onPropChange('country', value);
  //   this.onPropChange('city', '');
  //   this.props.getCities(value);
  // }

  format(date) {
    result = '';
    //console.log(date);
    hours = date.getHours();
    meridiem = hours < 12 ? 'AM' : 'PM';
    if (hours === 0) hours = 12;
    if (hours > 12) hours -= 12;
    if (hours < 10) hours = '0' + hours;
    minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return hours + ':' + minutes + ' ' + meridiem;
  }

  handleFromPickerConfirm(time, day) {

    const { operation_hours } = this.state;
    var dayFromState = day+'from';
    var newOpHours = operation_hours.map(operation_hour => {
      if (day === Object.keys(operation_hour)[0]) {
        const operation_hour_format = operation_hour[day].split('/');
        return {[day]:time+'/'+operation_hour_format[1]};
      } else {
        return operation_hour;
      }
    });
    //console.log(newOpHours);
    this.onPropChange('operation_hours', newOpHours);
    this.setState({[dayFromState]: false });
  }

  handleToPickerConfirm(time, day) {
    const { operation_hours } = this.state;
    var dayToState = day+'to';
    var newOpHours = operation_hours.map(operation_hour => {
      if (day === Object.keys(operation_hour)[0]) {
        const operation_hour_format = operation_hour[day].split('/');
        return {[day]:operation_hour_format[0]+'/'+time};
      } else {
        return operation_hour;
      }
    });
    this.onPropChange('operation_hours', newOpHours);
    this.setState({[dayToState]: false });
  }

  render() {
    const {
      scrollViewStyle,
      imageStyle,
      containerStyle,
      addPhotoStyle,
      photoStyle,
      textContainerStyle,
      textStyle,
      selectStyle,
      timeStyle,
      signUpStyle
    } = styles;
    const {
    //  area,
      operationHours,
      flags,
      region,
      fromTime,
      toTime,
      flagsModalVisible
    } = this.state;
    const {
      flagsModal,
      regionModal
    } = this.refs;
    const fields = [
      { name: 'address', placeholder: 'Address' },
      { name: 'addressAr', placeholder: 'Arabic address' },
      { name: 'phone', placeholder: 'Phone number' },
      { name: 'lat', placeholder: 'Latitude' },
      { name: 'lng', placeholder: 'Longitude' }
    ];
  //   {this.renderPhoto()} //this line was below this line -> <CardSection style={containerStyle}>
 //<KeyboardAvoidingView behavior={OS === 'ios' ? "padding" : null}>
     
    return (
      <View style={{flex: 1}}>
        <ScrollView style={scrollViewStyle}>
          <Card style={imageStyle}>
          <CardSection style={containerStyle}>
            <Text type="dark" textAlign="left"> {this.props.branch?this.state.name:this.props.business.name}</Text>
          </CardSection>
          <CardSection style={containerStyle}> 
              {fields.map(field => {
                 if (field.name === 'lat' || field.name === 'lng' || field.name === 'phone') {
                  return (
                  <Input
                    key={field.name}
                    value={this.state[field.name]}
                    onChangeText={this.onPropChange.bind(this, field.name)}
                    placeholder={field.placeholder}
                    type="dark"
                    textAlign="left"
                    style = {{marginTop:11}}
                    multiline={field.multiline}
                    keyboardType = "phone-pad"
                  />
                );}
                return (
                  <Input
                    key={field.name}
                    value={this.state[field.name]}
                    onChangeText={this.onPropChange.bind(this, field.name)}
                    placeholder={field.placeholder}
                    type="dark"
                    textAlign="left"
                    style = {{marginTop:11}}
                    multiline={field.multiline}
                  />
                );
              })}
        
              <Text style={{ flex: 1 }}>Operation hours</Text>
              {this.renderOperationHours()}
              <TouchableOpacity
                style={timeStyle}
                onPress={() => flagsModal.open()}
              >
                <Text>Choose flags ({flags.length} selected)</Text>
              </TouchableOpacity>
              {this.renderButton()}
            </CardSection>
          </Card>
        </ScrollView>
        <Modal
          ref="flagsModal"
          onRequestClose={() => null}
          style={{ height: '50%' }}
          backdrop
        >
          <SelectMultiple
            items={this.props.flags.map(flag => {
              return { label: flag.name || flag.label, value: flag.id || flag.value };
            })}
            selectedItems={flags.map(flag => {
              return { label: flag.name || flag.label, value: flag.id || flag.value };
            })}
            onSelectionsChange={flags => this.onPropChange('flags', flags.map(flag => {
              return { label: flag.name || flag.label, value: flag.id || flag.value };
            }))}
          />
        </Modal>
        <Modal
          ref="regionModal"
          onRequestClose={() => null}
          style={{ height: '50%' }}
          backdrop
          swipeToClose={false}
        >
          <MapView
            style={{ width: '100%', height: '100%' }}
            initialRegion={region}
            onRegionChange={region => this.onPropChange('region', region)}
          />
        </Modal>
           <View style={{position: 'absolute', left: 0, right: 0, bottom: 0}}>
        <NavigationBar currentScene = {'add'} />
        </View>
        </View>
      
    );
  }
}
 //</KeyboardAvoidingView>
              // <Picker
              //   style={selectStyle}
              //   selectedValue={area}
              //   onValueChange={(value, index) => this.onPropChange('area', value)}
              // >
              //   <Picker.Item label="Choose Area" value="" />
              //   {this.renderAreas()}
              // </Picker>
const styles = {
  scrollViewStyle: {
    marginBottom: 5
  },
  imageStyle: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  containerStyle: {
    display: 'flex',
  	flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  addPhotoStyle: {
    width: '100%',
    height: 120,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#242424',
    marginBottom: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoStyle: {
  	width: '100%',
    height: 120,
    resizeMode: 'contain',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  textContainerStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  textStyle: {
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 10
  },
  selectStyle: {
    flex: 1,
    width: '100%',
    height: OS === 'android' ? 40 : undefined,
    borderRadius: 10,
    backgroundColor: '#f9f9f9',
    marginBottom: 10,
    shadowColor: '#7c7c7c',
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.4,
    elevation: 2
  },
  timeStyle: {
    padding: 5,
    borderRadius: 10,
    backgroundColor: '#f9f9f9',
    marginBottom: 10
  },
  signUpStyle: {
    textDecorationLine: 'underline'
  }
};

const mapStateToProps = ({ businessForm }) => {
  const {
    areas,
    flags,
    error,
    loading
  } = businessForm;
  return {
    areas,
    flags,
    error,
    loading
  };
};

export default connect(mapStateToProps, {
  getAreas,
  getFlags,
  submitBranch
})(BranchForm);
