import React, { Component } from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  View,
  ListView,
  RefreshControl,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { getBranches } from '../actions';
import BranchItem from './BranchItem';
import SearchBarBranches from './SearchBarBranches';
import NavigationBar from './NavigationBar';
import { Actions } from 'react-native-router-flux';

const { OS } = Platform;
//6672
class BranchDashboard extends Component {
  
  componentWillMount() {
  	const { getBranches, business, branches } = this.props;
    getBranches(business.id);
    this.createDataSource(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.createDataSource(nextProps);
  }

  createDataSource({ keyword, branches }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    //console.log(branches);
    this.dataSource = ds.cloneWithRows(branches.filter(branch => {
      //console.log(branch);
      return branch.area.name.toLowerCase().indexOf(keyword.toLowerCase()) != -1;
    }));
  }
//BranchItem should be created here
  renderRow(branch) {
    return <BranchItem branch={branch} onSubmit={this.onRefresh.bind(this)} />;
  }

  onRefresh() {
    const { getBranches, business } = this.props;
    getBranches(business.id);
    this.createDataSource(this.props);
  }

  renderListView() {
    const { containerStyle, listViewStyle, textStyle } = styles;
    const onSubmit = this.onRefresh.bind(this);
  	return (
      <View style={containerStyle}>
        <ListView
          style={listViewStyle}
          bounces={false}
        	enableEmptySections
          renderHeader={() => <SearchBarBranches />}
          stickyHeaderIndices={[]}
          dataSource={this.dataSource}
          renderRow={this.renderRow.bind(this)}
          refreshControl={
            <RefreshControl
              refreshing={this.props.loading}
              onRefresh={this.onRefresh.bind(this)}
            />
          }
        />
        <NavigationBar business = {this.props.business} onSubmit = {onSubmit} currentScene = {'home'} />
      </View>
    );
  }

   // onRight={() => {Actions.businessForm({ onSubmit: () => getBusinesses() })}}
   //          rightButtonImage={require('./assets/add_icon.png')}

  render() {
    //console.log(this.props.branches);
    return (
      <KeyboardAvoidingView behavior={OS === 'ios' ? "padding" : null}>
        {this.renderListView()}
      </KeyboardAvoidingView>
    );
  }
}
//

const styles = {
  containerStyle: {
    display: 'flex',
    backgroundColor: 'white',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  listViewStyle: {
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    marginLeft: 5,
    marginRight: 5,
    paddingTop: 5,
    marginBottom: 5,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15
  },
  textStyle: {
    color: 'black',
    marginRight: 5
  }
};

const mapStateToProps = ({ branch }) => {
	const { branches , error, loading, keyword } = branch;
  return { branches, error, loading, keyword };
};

export default connect(mapStateToProps, { getBranches })(BranchDashboard);
