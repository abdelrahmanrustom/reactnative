import React, { Component } from 'react';
import { TouchableHighlight, Image, Text, View, Linking, Platform, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from './common';
import { connect } from 'react-redux';
import { getLoginState } from '../actions';
//import { AppInstalledChecker, CheckPackageInstallation } from 'react-native-check-app-install';
const { OS } = Platform;
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class ChooseAppScreen extends Component {
	
	// componentWillMount() {
	// const windowWidth = Dimensions.get('window').width;
	// const windowHeight = Dimensions.get('window').height;
	// console.log("width "+ windowWidth + " height "+ windowHeight );	
 //  	}

  componentWillMount() {
    this.props.getLoginState();
  }


launchBlabber() {
	if( OS !== 'ios'){ 
		Linking.canOpenURL('app://myblabber')
		    .then(supported => {
		    	if (supported) {
		    		Linking.openURL('app://myblabber');
		    	} else {
		    		Linking.openURL('https://play.google.com/store/apps/details?id=com.blabber.app');
		    	}

		    	console.log('Success ', supported); })
		    .catch(err => Linking.openURL('https://play.google.com/store/apps/details?id=com.blabber.app'));
	} 
	else {
		//Linking.openURL('itms-apps://itunes.apple.com/app/id1194725392');
		Linking.canOpenURL('com.myblabber.blabber://')
		    .then(supported => {
		    	if (supported) {
					Linking.openURL('com.myblabber.blabber://');
					
					
		    	} else {
		    		Linking.openURL('itms-apps://itunes.apple.com/app/id1194725392');
		    	}

		    	console.log('Success ', supported); })
		    .catch(err => Linking.openURL('itms-apps://itunes.apple.com/app/id1194725392'));
	}
}

launchBusiness() {
	Actions.login({ type: 'replace' })
}

// <Text style={textStyle}>Are you a business ?</Text>

	render() {
		const { imageStyle, imageStyleIOS, boldTextStyle, textStyle } = styles;
		return (

		<View style={{
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                height: '100%',
                backgroundColor: 'white'
        }}>
			<TouchableHighlight  onPress={this.launchBusiness.bind(this)}>
		        <Image
		          style={OS === 'ios'? imageStyleIOS: imageStyle}
		          source={{uri: 'login_logo'}}
		          resizeMode={OS === 'ios'? null : Image.resizeMode.center}
		        />
	        </TouchableHighlight>

	        <Button color = '#623DD1' onPress={this.launchBusiness.bind(this)}
	        style={{ width: '80%', alignSelf: 'center', marginTop: 15, marginBottom: 30 }}
	        >Continue as a Business</Button>

	        <TouchableHighlight  style={{ marginTop: 30 }} onPress={this.launchBlabber.bind(this)}>
		        <Image
		          style={OS === 'ios'? imageStyleIOS: imageStyle}
				  source={{uri: 'blabber_app'}}
		          resizeMode={OS === 'ios'? null : Image.resizeMode.center}
		        />
	        </TouchableHighlight>

	        <Button color = '#2F87DE'
	        style={{ width: '80%', alignSelf: 'center', marginTop: 15 }}
	        onPress={this.launchBlabber.bind(this)}
	        >Continue as a User	in Blabber</Button>
      </View>

			
		);
	}
}

// { type: 'replace' }

const styles = {
	imageStyle: {
  	width: 100,
    height: 100
	},

	imageStyleIOS: {
		width: windowWidth * 0.31,
    	height: windowHeight * 0.16
	},
	boldTextStyle: {
		fontWeight: 'bold',
		marginTop: '30%'
	},
	textStyle: {
		fontFamily: 'Montserrat-Regular',
		color: '#292929',
		backgroundColor: 'transparent'
	}
}

export default connect(null, { getLoginState })(ChooseAppScreen);