import React from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Linking
} from 'react-native';
import { Card, CardSection } from './common';
import { Actions } from 'react-native-router-flux';

const NotificationItem = ({ notification }) => {
  const {
    containerStyle,
    imageStyle,
    notificationStyle,
    textStyle,
    userStyle,
    businessStyle
  } = styles;
  //console.log(notification);
  const { data } = notification;
  const { review_id, media_id, notifications_assets, user_data } = data.payload;
  const userURL = 'http://myblabber.com/web/user/' + user_data.id;
  //const businessURL = 'http://myblabber.com/web/business/' + business_data.id;
  const businessURL = 'http://myblabber.com/business-detail.php?id=' + notifications_assets.business_id+'&branch_id='+notifications_assets.branch_id;
  //const Touchable = body.indexOf('checked') !== -1 || body.indexOf('saved') !== -1 ? TouchableWithoutFeedback : TouchableOpacity;
  const Touchable = notification.seen === 1 ? TouchableWithoutFeedback : TouchableOpacity;
 // console.log(notification);
  var itemText;
  if (data.type === 'review') {
    itemText = <Text style={[textStyle, businessStyle]} 
    onPress={() => Linking.openURL(businessURL)}> posted a review on {notifications_assets.business_name}</Text>;
  }
   else {
    if (data.type === 'media') {
      itemText = <Text style={[textStyle, businessStyle]} 
    onPress={() => Linking.openURL(businessURL)}> posted a media on {notifications_assets.business_name}</Text>;
    } else {
     if (data.type === 'favorite') {
      itemText = <Text style={[textStyle, businessStyle]} 
      onPress={() => Linking.openURL(businessURL)}> saved {notifications_assets.business_name}</Text>;
      } 
    }
  }
  //<Text style={[textStyle, userStyle]} onPress={() => Linking.openURL(userURL)}>{user_data.name}</Text>    
  return (
    <Touchable onPress={() => {
      if (data.type === 'review') Actions.reviewItem({ review_id });
      if (data.type === 'media') Actions.mediaItem({ media_id });
    }}>
      <View style={containerStyle}>
        <Image style={imageStyle} source={{ uri: user_data.profile_photo }} />
        <Text style={[textStyle, notificationStyle]}>
          <Text style={[textStyle, userStyle]} >{user_data.name}</Text>    
          {itemText}
        </Text>
      </View>
    </Touchable>
  );
};

// const item = renderText() {
//   const {
//     textStyle,
//     businessStyle
//     } = styles;

//       return <Text style={[textStyle, businessStyle]} onPress={() => Linking.openURL(businessURL)}>{notifications_assets.business_name}</Text>;
//     //return <Button onPress={this.onButtonPress.bind(this)}>{business ? 'UPDATE' : 'CREATE'}</Button>;
//   }

const styles = {
  containerStyle: {
    borderRadius: 15,
    shadowColor: '#7c7c7c',
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.4,
    elevation: 2,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
    marginBottom: 5,
    padding: 10,
    backgroundColor: '#f9f9f9',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  imageStyle: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 10
  },
  notificationStyle: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  textStyle: {
    backgroundColor: 'transparent',
    color: '#a4a4a4'
  },
  userStyle: {
    color: '#0091c9'
  },
  businessStyle: {
    color: '#5d5d5d'
  }
};

export default NotificationItem;