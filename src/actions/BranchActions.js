import {
  GET_BRANCHES,
  GET_BRANCHES_SUCCESS,
  GET_BRANCHES_FAIL,
  DELETE_BRANCH,
  DELETE_BRANCH_SUCCESS,
  DELETE_BRANCH_FAIL,
  FILTER_BRANCHES
} from './types';
import axios from 'axios';
import SInfo from 'react-native-sensitive-info';
import Toast from 'react-native-simple-toast';
import CarrierInfo from 'react-native-carrier-info';
import { baseURL } from '../config';

const api = axios.create({ baseURL });

const getBranchesSuccess = (dispatch, branches) => {
  dispatch({ type: GET_BRANCHES_SUCCESS, payload: branches });
};

const getBranchesFail = (dispatch, error) => {
  dispatch({ type: GET_BRANCHES_FAIL, payload: error });
  Toast.show(error);
};

export const getBranches = business_id => {
  return async dispatch => {
    dispatch({ type: GET_BRANCHES });
    const user = JSON.parse(await SInfo.getItem('user', {}));
    api.post('get-branches', {
      user_id: user.id,
      auth_key: user.auth_key,
      business_id
    })
    .then(response => {
      if (response.data.status) {
        return getBranchesFail(dispatch, response.data.errors);
      }
      //console.log('success get branches ' + response.data);
      return getBranchesSuccess(dispatch, response.data.branches);
    })  
    .catch(() => getBranchesFail(dispatch, 'Get businesses failed'));
  };
};


const deleteBranchSuccess = (dispatch, business_id) => {
  dispatch({ type: DELETE_BRANCH_SUCCESS });
  dispatch(getBranches(business_id));
};

const deleteBranchFail = (dispatch, error) => {
  dispatch({ type: DELETE_BRANCH_FAIL, payload: error });
  Toast.show(error);
};

export const deleteBranch = (branch_id, business_id) => {
  return async dispatch => {
    dispatch({ type: DELETE_BRANCH });
    const user = JSON.parse(await SInfo.getItem('user', {}));
    api.post('delete-branch', {
      user_id: user.id,
      auth_key: user.auth_key,
      branch_id
    })
    .then(response => {
      if (response.data.status) {
         //console.log( response.data);
        return deleteBusinessFail(dispatch, response.data.errors);
      }
       //console.log( 'success ' + response.data+' branch_id '+ branch_id+ ' business_id: '+ business_id);
      return deleteBranchSuccess(dispatch, business_id);
    })
    .catch(() => deleteBranchFail(dispatch, 'Delete branch failed'));
  };

};

export const filterBranches = keyword => {
  return dispatch => dispatch({ type: FILTER_BRANCHES, payload: keyword });
};
