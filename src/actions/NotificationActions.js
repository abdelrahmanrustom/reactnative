import {
  GET_NOTIFICATIONS,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAIL,
  GET_REVIEW,
  GET_REVIEW_SUCCESS,
  GET_REVIEW_FAIL,
  GET_MEDIA,
  GET_MEDIA_SUCCESS,
  GET_MEDIA_FAIL,
  GET_USERS,
  GET_USERS_SUCCESS,
  GET_USERS_FAIL,
  GET_REVIEWS,
  GET_REVIEWS_SUCCESS,
  GET_REVIEWS_FAIL,
  GET_MEDIA_LIST,
  GET_MEDIA_LIST_SUCCESS,
  GET_MEDIA_LIST_FAIL,
  GET_COMMENTS,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAIL,
  ADD_COMMENT,
  ADD_COMMENT_SUCCESS,
  ADD_COMMENT_FAIL
} from './types';
import axios from 'axios';
import SInfo from 'react-native-sensitive-info';
import Toast from 'react-native-simple-toast';
import { baseURL } from '../config';

const api = axios.create({ baseURL });

const getNotificationsSuccess = (dispatch, data) => {
  dispatch({ type: GET_NOTIFICATIONS_SUCCESS, payload: data });
};

const getNotificationsFail = (dispatch, error) => {
  dispatch({ type: GET_NOTIFICATIONS_FAIL, payload: error });
  Toast.show(error);
};

export const getNotifications = (page) => {
  return async dispatch => {
    dispatch({ type: GET_NOTIFICATIONS });
    const user = JSON.parse(await SInfo.getItem('user', {}));
    api.post('get-notifications', {
      user_id: user.id,
      auth_key: user.auth_key,
      page
    })
    .then(response => {
      if (response.data.status) {
        return getNotificationsFail(dispatch, response.data.errors);
      }

      const notifications = response.data.notifications.filter(notification => {
        const { type } = notification.data;
        return type === 'review' || type === 'media' || type === 'checkin' || type === 'favorite' || type === 'comment';
      })
      const data = {
        'notifications': notifications,
        'pagination': response.data.pagination
      }
      console.log(data);
      return getNotificationsSuccess(dispatch, data);
    })  
    .catch((error) => {
      getNotificationsFail(dispatch, 'Get notifications failed')
    });
  };
};

const getReviewSuccess = (dispatch, review) => {
  dispatch({ type: GET_REVIEW_SUCCESS, payload: review });
};

const getReviewFail = (dispatch, error) => {
  dispatch({ type: GET_REVIEW_FAIL, payload: error });
  Toast.show(error);
};

export const getReview = review_id => {
  return dispatch => {
    dispatch({ type: GET_REVIEW });
    api.post('get-review', { review_id })
    .then(response => {
      if (response.data.status) {
        return getReviewFail(dispatch, response.data.errors);
      }
      return getReviewSuccess(dispatch, response.data.review);
    })  
    .catch(() => getReviewFail(dispatch, 'Get review failed'));
  };
}

const getUsersSuccess = (dispatch, users) => {
  dispatch({ type: GET_USERS_SUCCESS, payload: users });
};

const getUsersFail = (dispatch, error) => {
  dispatch({ type: GET_USERS_FAIL, payload: error });
  Toast.show(error);
};

export const getUsers = (branch_or_bussiness, business_id, list_type) => {
  return async dispatch => {
    dispatch({ type: GET_USERS });
    const user = JSON.parse(await SInfo.getItem('user', {}));
    //const key = type === 'checkins' ? 'business_id_to_get' : 'business_to_get';
    //const key = 'business_id' ;
   // console.log('branch_id'+ business_id+ ' type '+ branch_or_bussiness+ ' list_type '+ list_type);
    api.post(`get-${list_type}`, { [branch_or_bussiness]: business_id, user_id: user.id, auth_key: user.auth_key })
    .then(response => {
      if (response.data.status) {
        return getUsersFail(dispatch, response.data.errors);
      }
      //console.log( response.data);
      const users = list_type === 'checkins'
      ? response.data.checkins.map(checkin => checkin.user)
      : response.data.users;
      //console.log(response.data);
      //console.log(users);
      return getUsersSuccess(dispatch, users);
    })
    .catch(() => getUsersFail(dispatch, 'Get users failed'));
  };
}

const getReviewsSuccess = (dispatch, reviews) => {
  dispatch({ type: GET_REVIEWS_SUCCESS, payload: reviews });
};

const getReviewsFail = (dispatch, error) => {
  dispatch({ type: GET_REVIEWS_FAIL, payload: error });
  Toast.show(error);
};

export const getReviews = business_id_to_get => {
  return async dispatch => {
    dispatch({ type: GET_REVIEWS });
    const user = JSON.parse(await SInfo.getItem('user', {}));
    api.post('get-reviews', { branch_id: business_id_to_get, user_id: user.id, auth_key: user.auth_key })
    .then(response => {
      if (response.data.status) {
        return getReviewsFail(dispatch, response.data.errors);
      }
      return getReviewsSuccess(dispatch, response.data.reviews);
    })  
    .catch(() => getReviewsFail(dispatch, 'Get reviews failed'));
  };
}

const getMediaListSuccess = (dispatch, users) => {
  dispatch({ type: GET_MEDIA_LIST_SUCCESS, payload: users });
};

const getMediaListFail = (dispatch, error) => {
  dispatch({ type: GET_MEDIA_LIST_FAIL, payload: error });
  Toast.show(error);
};

export const getMediaList = (business_id, branch_id) => {
  return async dispatch => {
    dispatch({ type: GET_MEDIA_LIST });
    const user = JSON.parse(await SInfo.getItem('user', {}));
  //  console.log( business_id  + ' id '+ branch_id);
    //  business_id: business? business_to_get: undefined, user_id: user.id + auth_key: user.auth_key );

    api.post('get-media', { branch_id,
     business_id, user_id: user.id, auth_key: user.auth_key })
    .then(response => {
      if (response.data.status) {
       // console.log( response.data.errors);
        return getMediaListFail(dispatch, response.data.errors);
      }
      //console.log("tamam "+response.data.media);
      return getMediaListSuccess(dispatch, response.data.media);
    })  
    .catch(() => getMediaListFail(dispatch, 'Get media list failed'));
  };
}

const getMediaSuccess = (dispatch, media) => {
  // console.log(" success"+ media);
  dispatch({ type: GET_MEDIA_SUCCESS, payload: media });
};

const getMediaFail = (dispatch, error) => {
  dispatch({ type: GET_MEDIA_FAIL, payload: error });
  Toast.show(error);
};

export const getMedia = id => {
  
  return async dispatch => {
    dispatch({ type: GET_MEDIA });
   // console.log("media_id" + id);
   // const user = JSON.parse(await SInfo.getItem('user', {}));
    api.post('get-media-by-ids', { id })
    //api.get(`get-media-by-ids?id=${id}`)
    .then(response => {
     // console.log(id+" error"+ response.data.status);
      if (response.data.status) {
      //  console.log(id+" error"+ response.data.errors);
        return getMediaFail(dispatch, response.data.errors);
      }
     
      return getMediaSuccess(dispatch, response.data.media[0]);
    })  
    .catch(() => getMediaFail(dispatch, 'Get media failed'));
    //});
    // console.log(" catch "  );
  };
}

const getCommentsSuccess = (dispatch, comment) => {
  dispatch({ type: GET_COMMENTS_SUCCESS, payload: comment });
};

const getCommentsFail = (dispatch, error) => {
  dispatch({ type: GET_COMMENTS_FAIL, payload: error });
  Toast.show(error);
};

export const getComments = (object_id, object_type) => {
 // console.log('getcomments '+ object_type+ ' object_id '+object_id);
  return async dispatch => {
    dispatch({ type: GET_COMMENTS });
    const user = JSON.parse(await SInfo.getItem('user', {}));
    console.log(object_id+ ' object type '+ object_type);
    api.post('get-comments', {
      user_id: user.id,
      auth_key: user.auth_key,
      object_id,
      object_type
    })
    .then(response => {
      if (response.data.status) {
     //   console.log('getCommentfails '+ response.data.errors);
        return getCommentsFail(dispatch, response.data.errors);
      }
      //console.log('getCommentsucced '+ response.data.comments);
      return getCommentsSuccess(dispatch, response.data.comments);
    })  
    .catch((error) =>{
      //console.log('item fails'); 
      getCommentsFail(dispatch, 'Get comments failed')
    });
  };
}

const addCommentSuccess = (dispatch, object_id, object_type) => {
  dispatch({ type: ADD_COMMENT_SUCCESS });
  dispatch(getComments(object_id, object_type));
};

const addCommentFail = (dispatch, error) => {
  dispatch({ type: ADD_COMMENT_FAIL, payload: error });
  Toast.show(error);
};

export const addComment = (text, object_id, object_type, business_identity) => {
  return async dispatch => {
    dispatch({ type: ADD_COMMENT });
    const user = JSON.parse(await SInfo.getItem('user', {}));
    //console.log('addComment -> objecy_id: '+ object_id+' object_type: '+ object_type+ ' business_identity '+ business_identity);
    api.post('comment', {
      user_id: user.id,
      auth_key: user.auth_key,
      text,
      object_id,
      object_type,
      business_identity
    })
    .then(response => {
      if (response.data.status) {
        console.log( response.data);
        return addCommentFail(dispatch, response.data.errors);
      }
      console.log('success object_id: '+ object_id+' object_type: '+ object_type);
      return addCommentSuccess(dispatch, object_id, object_type);
    })  
    .catch((error) => {
      console.log(error);
      getCommentsFail(dispatch, 'Get comments failed')});
  };
}