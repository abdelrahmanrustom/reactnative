import {
  PROP_CHANGED,
  GET_COUNTRIES,
  GET_COUNTRIES_SUCCESS,
  GET_COUNTRIES_FAIL,
  GET_CITIES,
  GET_CITIES_SUCCESS,
  GET_CITIES_FAIL,
  GET_AREAS,
  GET_AREAS_SUCCESS,
  GET_AREAS_FAIL,
  GET_CATEGORIES,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAIL,
  GET_SUBCATEGORIES,
  GET_SUBCATEGORIES_SUCCESS,
  GET_SUBCATEGORIES_FAIL,
  GET_FLAGS,
  GET_FLAGS_SUCCESS,
  GET_FLAGS_FAIL,
  GET_INTERESTS,
  GET_INTERESTS_SUCCESS,
  GET_INTERESTS_FAIL,
  SUBMIT_BUSINESS,
  SUBMIT_BUSINESS_SUCCESS,
  SUBMIT_BUSINESS_FAIL,
  SUBMIT_BRANCH,
  SUBMIT_BRANCH_SUCCESS,
  SUBMIT_BRANCH_FAIL,
  SUBMIT_MEDIA,
  SUBMIT_MEDIA_SUCCESS,
  SUBMIT_MEDIA_FAIL,
  DELETE_MEDIA,
  DELETE_MEDIA_SUCCESS,
  DELETE_MEDIA_FAIL
} from './types';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { Keyboard, Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import SInfo from 'react-native-sensitive-info';
import { baseURL } from '../config';

const api = axios.create({ baseURL });

const getCountriesSuccess = (dispatch, countries) => {
  dispatch({ type: GET_COUNTRIES_SUCCESS, payload: countries });
};

const getCountriesFail = (dispatch, error) => {
  dispatch({ type: GET_COUNTRIES_FAIL, payload: error });
  Toast.show(error);
};

export const getCountries = () => {
  return dispatch => {
    dispatch({ type: GET_COUNTRIES });
    api.get(`get-countries?page=${2}`)
    .then(response => {
      if (response.data.status) {
        return getCountriesFail(dispatch, response.data.errors);
      }
     // console.log(response.data.countries);
      return getCountriesSuccess(dispatch, response.data.countries);
    })  
    .catch(() => getCountriesFail(dispatch, 'Get countries failed'));
  };
};

const getCitiesSuccess = (dispatch, cities) => {
  dispatch({ type: GET_CITIES_SUCCESS, payload: cities });
};

const getCitiesFail = (dispatch, error) => {
  dispatch({ type: GET_CITIES_FAIL, payload: error });
  Toast.show(error);
};

export const getCities = countryId => {
  return dispatch => {
    dispatch({ type: GET_CITIES });
    api.get(`get-cities?country_id=${countryId}`)
    .then(response => {
      if (response.data.status) {
        return getCitiesFail(dispatch, response.data.errors);
      }
      return getCitiesSuccess(dispatch, response.data.cities);
    })  
    .catch(() => getCitiesFail(dispatch, 'Get cities failed'));
  };
};



const getAreasSuccess = (dispatch, cities) => {
  dispatch({ type: GET_AREAS_SUCCESS, payload: cities });
};

const getAreasFail = (dispatch, error) => {
  dispatch({ type: GET_AREAS_FAIL, payload: error });
  Toast.show(error);
};

export const getAreas = cityId => {
  //console.log(cityId);
  return dispatch => {
    //console.log('ahmed');
    dispatch({ type: GET_AREAS });
    //api.get(`get-areas?city_id=${cityId}`)
    api.post('get-areas', {city_id: cityId})
    .then(response => {
      if (response.data.status) {
        //console.log('error '+ response.data.areas);
        return getAreasFail(dispatch, response.data.errors);
      }
      //response.data.areas.map(area => console.log(area.name));
      //console.log('sucess '+ response.data.areas);
      return getAreasSuccess(dispatch, response.data.areas);
    })  
    .catch(() => getAreasFail(dispatch, 'Get Areas failed'));
  };
};

const getCategoriesSuccess = (dispatch, categories) => {
  dispatch({ type: GET_CATEGORIES_SUCCESS, payload: categories });
};

const getCategoriesFail = (dispatch, error) => {
  dispatch({ type: GET_CATEGORIES_FAIL, payload: error });
  Toast.show(error);
};

export const getCategories = () => {
  return dispatch => {
    dispatch({ type: GET_CATEGORIES });
    api.get('get-categories')
    .then(response => {
      if (response.data.status) {
        return getCategoriesFail(dispatch, response.data.errors);
      }
      return getCategoriesSuccess(dispatch, response.data.categories);
    })  
    .catch(() => getCategoriesFail(dispatch, 'Get categories failed'));
  };
};

const getSubcategoriesSuccess = (dispatch, subcategories) => {
  dispatch({ type: GET_SUBCATEGORIES_SUCCESS, payload: subcategories });
};

const getSubcategoriesFail = (dispatch, error) => {
  dispatch({ type: GET_SUBCATEGORIES_FAIL, payload: error });
  Toast.show(error);
};

export const getSubcategories = categoryId => {
  return dispatch => {
    dispatch({ type: GET_SUBCATEGORIES });
    api.get(`get-categories?category_id=${categoryId}`)
    .then(response => {
      if (response.data.status) {
        return getSubcategoriesFail(dispatch, response.data.errors);
      }
      return getSubcategoriesSuccess(dispatch, response.data.categories);
    })  
    .catch(() => getSubcategoriesFail(dispatch, 'Get subcategories failed'));
  };
};

const getFlagsSuccess = (dispatch, subcategories) => {
  dispatch({ type: GET_FLAGS_SUCCESS, payload: subcategories });
};

const getFlagsFail = (dispatch, error) => {
  dispatch({ type: GET_FLAGS_FAIL, payload: error });
  Toast.show(error);
};

export const getFlags = () => {
  return dispatch => {
    //console.log('ahmed');
    dispatch({ type: GET_FLAGS });
    api.get('get-flags')
    .then(response => {
      if (response.data.status) {
        return getFlagsFail(dispatch, response.data.errors);
      }
      //console.log('flags '+  response.data.flags);
      return getFlagsSuccess(dispatch, response.data.flags);
    })  
    .catch(() => getFlagsFail(dispatch, 'Get flags failed'));
  };
};

const getInterestsSuccess = (dispatch, interests) => {
  dispatch({ type: GET_INTERESTS_SUCCESS, payload: interests });
};

const getInterestsFail = (dispatch, error) => {
  dispatch({ type: GET_INTERESTS_FAIL, payload: error });
  Toast.show(error);
};

export const getInterests = () => {
  return dispatch => {
    dispatch({ type: GET_INTERESTS });
    api.get('get-interests')
    .then(response => {
      if (response.data.status) {
        return getInterestsFail(dispatch, response.data.errors);
      }
      return getInterestsSuccess(dispatch, response.data.interests);
    })  
    .catch(() => getInterestsFail(dispatch, 'Get interests failed'));
  };
};

const submitBusinessSuccess = (dispatch, onSubmit, business_id) => {
  dispatch({ type: SUBMIT_BUSINESS_SUCCESS });
  Keyboard.dismiss();
  onSubmit();
  Actions.pop();
  if (!business_id) {
    Toast.show('The business you just created is in process. Kindly wait for an approval.');
  }
};

const submitBusinessFail = (dispatch, error) => {
  dispatch({ type: SUBMIT_BUSINESS_FAIL, payload: error });
  Toast.show(error);
}


export const submitBusiness = (
  media,
  name,
  nameAr,
  phone,
  website,
  facebook,
  description,
  descriptionAr,
  category,
  price,
  interests,
  business_id = '',
  onSubmit
  ) => {
  return async dispatch => {
    const { id, auth_key } = JSON.parse(await SInfo.getItem('user', {}));
    dispatch({ type: SUBMIT_BUSINESS });
    let data = new window.FormData();
    media && data.append('Media[file]"; filename="photo.jpg"', media);
   console.log("submit business");
    data.append('name', name),
    data.append('nameAr', nameAr),
    data.append('phone', phone),
    data.append('website', website),
    data.append('fb_page', facebook),
    data.append('description', description),
    data.append('descriptionAr', descriptionAr),
    data.append('category_id', category),
    data.append('price', price),
    data.append('interests', interests),
    data.append('user_id', id);
    data.append('auth_key', auth_key);
    business_id && data.append('business_id', business_id);
    const config = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      }
    }
    console.log(data);
    api.post((business_id ? 'edit' : 'add') + '-business', data, config)
    .then(response => {
      const { status, errors } = response.data;
      if (status) {
        return submitBusinessFail(dispatch, errors);
      }
      return submitBusinessSuccess(dispatch, onSubmit, business_id);
    })
    .catch(() => submitBusinessFail(dispatch, 'Business submit failed'));
  };
};



const submitBranchSuccess = (dispatch, onSubmit, branch_id) => {
  dispatch({ type: SUBMIT_BRANCH_SUCCESS });
  Keyboard.dismiss();
  console.log('submitBranchSuccess');
  onSubmit();
 // console.log('submitBranchSuccess');
  Actions.pop();
};

const submitBranchFail = (dispatch, error) => {
 // console.log('submitBranchFail');
  dispatch({ type: SUBMIT_BRANCH_FAIL, payload: error });
  Toast.show(error);
}


export const submitBranch = (
  address,
  addressAr,
  phone,
  op_hours_to_be_submitted,
  flags,
  latitude,
  longitude,
  branch_id = '',
  business_id,
  onSubmit
) => {
  return async dispatch => {
    const { id, auth_key } = JSON.parse(await SInfo.getItem('user', {}));
    dispatch({ type: SUBMIT_BRANCH });
    // console.log('name: '+ name+ ' nameAr: '+ nameAr + ' address: ' + address+ ' addressAr: '+
    //   addressAr+ ' city: '+ city+ ' phone: '+ phone+ ' Operation: '+ op_hours_to_be_submitted+ ' lat: '+ latitude+ ' flags: '+ flags+
    //   ' branch_id '+ branch_id  );
    //console.log('lat: '+ latitude + ' long: '+ longitude);
    let data = {
      address,
      addressAr,
      phone,
      operation_hours: op_hours_to_be_submitted,
      flag_ids: flags,
      lat: latitude,
      lng: longitude,
      branch_id,
      business_id,
      user_id: id,
      auth_key: auth_key,
    }
   console.log(data);
    api.post((branch_id ? 'edit' : 'add') + '-branch',data).then(response => {
      const { status, errors } = response.data;
      if (status) {
        console.log('error ' + errors);
        return submitBranchFail(dispatch, errors);
      }
      console.log(response.data);
      console.log('cool branch_id ' + branch_id);
      //return submitBranchFail(dispatch, 'A'+data.flag_ids+'A');
      return submitBranchSuccess(dispatch, onSubmit, branch_id? branch_id: response.data.branch_id);
    })
    .catch(() => submitBranchFail(dispatch, 'Branch submit failed'));
  };
};

const submitMediaSuccess = (dispatch, onSubmit) => {
  dispatch({ type: SUBMIT_MEDIA_SUCCESS });
  Keyboard.dismiss();
  onSubmit();
  Actions.pop();
};

const submitMediaFail = (dispatch, error) => {
  dispatch({ type: SUBMIT_MEDIA_FAIL, payload: error });
  Toast.show(error);
}

export const submitMedia = (business_id, branch_id, media, type, caption, rating, onSubmit) => {
  return async dispatch => {
    const { id, auth_key } = JSON.parse(await SInfo.getItem('user', {}));
    dispatch({ type: SUBMIT_MEDIA });
    let data = new window.FormData();
    data.append('user_id', id);
    data.append('auth_key', auth_key);
    data.append('business_id', business_id);
    data.append('branch_id', branch_id);
    data.append('type', type);
    if (type === 'brochure') {
    media && data.append('Media[file]"; filename="photo.pdf"', media);
    } else {
    media && data.append('Media[file]"; filename="photo.jpg"', media); 
    }
    data.append('caption', caption);
    data.append('rating', rating);
    console.log(data);
    const config = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      }
    }
    api.post('add-media', data, config)
    .then(response => {
      const { status, errors } = response.data;
      console.log(response.data);
      if (status) {
        return submitMediaFail(dispatch, errors);
      }
      return submitMediaSuccess(dispatch, onSubmit);
    })
    .catch( error => {
      console.log(error);
      submitMediaFail(dispatch, 'Media submit failed')
    });
  };
};

const deleteMediaSuccess = (dispatch, business_id, onSubmit) => {
  dispatch({ type: DELETE_MEDIA_SUCCESS });
  onSubmit();
};

const deleteMediaFail = (dispatch, error) => {
  dispatch({ type: DELETE_MEDIA_FAIL, payload: error });
  Toast.show(error);
}

export const deleteMedia = (media_id, business_id, onSubmit) => {
  return dispatch => {
    Alert.alert(
      '',
      'Are you sure you want to delete this image?',
      [
        { text: 'No' },
        {
          text: 'Yes',
          onPress: async () => {
            dispatch({ type: DELETE_MEDIA });
            const user = JSON.parse(await SInfo.getItem('user', {}));
            api.post('delete-media', {
              user_id: user.id,
              auth_key: user.auth_key,
              media_id
            })
            .then(response => {
              if (response.data.status) {
                return deleteMediaFail(dispatch, response.data.errors);
              }
              return deleteMediaSuccess(dispatch, business_id, onSubmit);
            })
            .catch(() => deleteMediaFail(dispatch, 'Logout failed'));
          }
        }
      ],
      { cancelable: false }
    );
  };
};
